package com.nkorth.mtglife;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";
	private ArrayList<Player> players;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(players == null){
            players = new ArrayList<Player>();
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor e = preferences.edit();

        String data = "";
        for(Player player : players){
            data += player.playerName + "%%%" + player.score + "@@@";
        }
        e.putString("players", data);
        Log.d(TAG, data);
        e.putInt("playersCreated", Player.playersCreated);
        Log.d(TAG, "Players created: "+Player.playersCreated);
        e.commit();
        Log.d(TAG, "Finished saving shared prefs");
    }

    @Override
    protected void onResume(){
        super.onResume();
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);

        String data = preferences.getString("players", "NONE");
        resetPlayers();
        if(!data.equals("NONE") && !data.equals("")){
            String[] playerStrings = data.split("@@@");
            for(String s : playerStrings){
                int c = s.lastIndexOf("%%%");
                String name = s.substring(0, c);
                int score = Integer.parseInt(s.substring(c + 3));
                addPlayer(name, score);
            }
            Log.d(TAG, "Finished loading shared prefs");
        } else if(data.equals("NONE")){
            // initialize with two players
            addPlayer(String.format(getString(R.string.player_num), 1), 20);
            addPlayer(String.format(getString(R.string.player_num), 2), 20);
            Log.d(TAG, "Initialized default players");
        }
        updatePlayerList();

        Player.playersCreated = preferences.getInt("playersCreated", 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.action_share_app).setIntent(new Intent(this, ShareActivity.class));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.action_add_player:
                addPlayerPrompt();
                return true;
            case R.id.action_add_counter:
                addCounterPrompt();
                return true;
            case R.id.action_reset:
                resetPrompt();
                return true;
            case R.id.action_roll_d20:
                rollD20();
                return true;
            case R.id.action_flip_coin:
                flipCoin();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void updatePlayerList(){
        // delete players from list if necessary
        for(int i = players.size() - 1; i >= 0; i--){
            if(players.get(i).toRemove){
                players.remove(i);
            }
        }
        // update "no players" message
        LinearLayout list = (LinearLayout) findViewById(R.id.player_list);
        View message = list.findViewById(R.id.noPlayers);
        if(list.getChildCount() == 0 && message == null){
            LayoutInflater inflater = getLayoutInflater();
            View noPlayersMessage = inflater.inflate(R.layout.noplayers, null);
            list.addView(noPlayersMessage);
        } else if(list.getChildCount() > 1 && message != null){
            list.removeView(message);
        }
    }

    public void addPlayer(String name, int score){
        LinearLayout list = (LinearLayout) findViewById(R.id.player_list);
        players.add(new Player(this, list, name, score));
        updatePlayerList();
    }

    public void addPlayerPrompt(){
        AlertDialog.Builder d = new AlertDialog.Builder(this);
        final EditText nameBox = new EditText(this);
        nameBox.setSingleLine();
        nameBox.setText(String.format(getString(R.string.player_num), Player.playersCreated + 1));
        nameBox.setHint(R.string.player_name);
        nameBox.setSelectAllOnFocus(true);
        d.setView(nameBox);
        d.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        d.setPositiveButton(R.string.add_player, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String name = nameBox.getText().toString();
                addPlayer(name, 20);
                dialogInterface.cancel();
            }
        });
        d.show();
    }

    public void addCounterPrompt(){
        AlertDialog.Builder d = new AlertDialog.Builder(this);
        final EditText nameBox = new EditText(this);
        nameBox.setSingleLine();
        nameBox.setHint(R.string.player_name);
        d.setView(nameBox);
        d.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        d.setPositiveButton(R.string.add_counter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String name = nameBox.getText().toString();
                addPlayer(name, 0);
                dialogInterface.cancel();
            }
        });
        d.show();
    }

    public void resetPlayers(){
        for(int i = players.size() - 1; i >= 0; i--){
            players.get(i).destroy();
        }
        Player.playersCreated = 0;
    }

    public void resetPrompt(){
        AlertDialog.Builder d = new AlertDialog.Builder(MainActivity.this);
        d.setTitle(R.string.reset_confirm_message);
        d.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                dialogInterface.cancel();
            }
        });
        d.setPositiveButton(R.string.reset_confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                resetPlayers();
                dialogInterface.cancel();
            }
        });
        d.show();
    }

    public void rollD20(){
        int result = 1 + (int)(Math.random() * 20);
        AlertDialog.Builder d = new AlertDialog.Builder(this);
        if(result == 8 || result == 11 || result == 18)
            d.setMessage(String.format(getString(R.string.d20_result_an), result));
        else
            d.setMessage(String.format(getString(R.string.d20_result), result));
        /*d.setNeutralButton(R.string.roll_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
                rollD20();

            }
        });*/
        d.setNegativeButton(R.string.dialog_dismiss, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        d.show();
    }

    public void flipCoin(){
        double random = Math.random();
        String result = (random >= 0.5) ? getString(R.string.coin_heads): getString(R.string.coin_tails);
        AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setMessage(String.format(getString(R.string.coin_result), result));
        /*d.setNeutralButton(R.string.flip_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
                flipCoin();
            }
        });*/
        d.setNegativeButton(R.string.dialog_dismiss, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        d.show();
    }
}