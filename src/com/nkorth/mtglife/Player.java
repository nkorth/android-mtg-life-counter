package com.nkorth.mtglife;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Player{
    final MainActivity context;
    LinearLayout parentView;
    View playerView;
    TextView scoreView;
    int score;
    final String playerName;
    boolean toRemove = false;

    public static int playersCreated = 0;
    private static final int[] colors = {
            Color.rgb(255, 0, 0),
            Color.rgb(255, 125, 0),
            Color.rgb(255, 255, 0),
            Color.rgb(0, 255, 50),
            Color.rgb(0, 255, 255),
            Color.rgb(0, 100, 255),
            Color.rgb(150, 0, 200),
            Color.rgb(255, 125, 200),
    };


    public Player(final MainActivity context, LinearLayout parent, String name, int initialScore){
        this.context = context;
        score = initialScore;
        playerName = name;
        parentView = parent;
        LayoutInflater inflater = context.getLayoutInflater();
        playerView = inflater.inflate(R.layout.player, null);
        parentView.addView(playerView);
        TextView nameView = (TextView) playerView.findViewById(R.id.player_name);
        nameView.setText(name);
        scoreView = (TextView) playerView.findViewById(R.id.player_score);
        scoreView.setTextColor(colors[playersCreated % colors.length]);
        playersCreated++;
        Button buttonPlus = (Button) playerView.findViewById(R.id.player_plus);
        Button buttonMinus = (Button) playerView.findViewById(R.id.player_minus);
        ImageButton buttonRemove = (ImageButton) playerView.findViewById(R.id.player_remove);
        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addScore();
            }
        });
        buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subtractScore();
            }
        });
        buttonRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder d = new AlertDialog.Builder(context);
                d.setTitle(String.format(context.getString(R.string.remove_confirm_message), playerName));
                d.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i){
                        dialogInterface.cancel();
                    }
                });
                d.setPositiveButton(R.string.remove_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        destroy();
                        dialogInterface.cancel();
                    }
                });
                d.show();
            }
        });
        updateView();
    }

    public void addScore(){
        score++;
        updateView();
    }

    public void subtractScore(){
        score--;
        updateView();
    }

    public void destroy(){
        parentView.removeView(playerView);
        toRemove = true;
        context.updatePlayerList();
    }

    private void updateView(){
        String scoreText = Integer.toString(score);
        scoreView.setText(scoreText);
//        Button buttonMinus = (Button) playerView.findViewById(R.id.player_minus);
//        if(score <= 0)
//            buttonMinus.setEnabled(false);
//        else
//            buttonMinus.setEnabled(true);
    }
}
